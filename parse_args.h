#ifndef _PARSE_ARGS_H
#define _PARSE_ARGS_H

int check_args();
int parse_args(int arg, char **argv);
int input_error(char *pgrm_name, char *err);
int usage();

#endif /*_PARSE_ARGS_H */
