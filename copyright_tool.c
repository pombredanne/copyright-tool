/******************************************************************************
*
* This program recursively adds or removes copyrights from source files. It 
* expects the phrasing to be such that the word "copyright" and the name of the
* company that holds the copyright are on the same line of the copyright
* clause.
*
* Works for most *.c, *.cpp, *.h, *.bat, *.inc, *.reg, *.cmn, *.bib
*
* Written by Brent Generous, bgenerous@adeneo-embedded.us
*
******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include "copyright_tool.h"
#include "dir_search.h"

/* GLOBALS to check whether mandantory option was supplied */
bool recursive	= false;
bool from_file	= false;
bool company	= false;

int remove_copyright(struct files_to_scrub filenames)
{
	return 0;
}

int save_changes()
{
	return 0;
}

int main(int argc, char *argv[])
{
	int ret = 0;
	int i = 0;
	char **files;
	char *company = "";
	struct files_to_scrub filenames;

	ret = parse_args(argc, argv);
	if (ret < 0) 
		goto exit;

	if (recursive) {
		filenames = locate_files(".");
		for (i = 0; i < filenames.file_count; i++) {
			printf("FILES TO SCRUB: %s\n", filenames.files[i]);
		}
	} else {
		files = NULL;
	}

exit:
	return ret;
}

