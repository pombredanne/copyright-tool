/******************************************************************************
*
* Functions to parse command line args. Used by Copyright tool.
*
* Written by Brent Generous, bgenerous@adeneo-embedded.us
*
******************************************************************************/ 

#include <stdio.h>
#include "copyright_tool.h"
#include "parse_args.h"

int check_args()
{
	int ret = 0;

	if (recursive && from_file) {
		printf("Must specify either recursive scrub or a file to scrub, but not both\n");
		ret = -1;
	} else if (!recursive && !from_file) {
		printf("Must specify either recursive scrub or a file to scrub, but not both\n");
		ret = -1;
	} else if (!company) {
		printf("Must specify company copyright to scrub\n");
		ret = -1;
	}

	return ret;
}

int parse_args(int argc, char **argv)
{
	int i;
	int ret = 0;

	if (argc == 1) {
		printf("%s: incorrect usage\n", argv[0]);
		input_error(argv[0], NULL);
		ret = -1;
		goto exit;
	}

	for (i = 1; i < argc; i++) {
		/* handle short args */
		if (strncmp((&argv[i][0]), "--", 2) != 0) {
			int c = argv[i][1];

			switch(c) {
				case 'c' :
					if (argv[i+1] == NULL) {
						input_error(argv[0], "-c, but no company name given");
						ret = -1;
						goto exit;
					} else {
						printf("company: %s\n", argv[++i]);
						company = true;
						break;
					}
				case 'f' :
					if (argv[i+1] == NULL) {
						input_error(argv[0], "-f, but no filename given");
						ret = -1;
						goto exit;
					} else {
						printf("file: %s\n", argv[++i]);
						from_file = true;
						break;
					}
				case 'r' :
					printf("recursive\n");
					recursive = true;
					break;
				case 'h' :
					usage();
					ret = -1;
					goto exit;
				default:
					input_error(argv[0], argv[i]);
					ret = -1;
					goto exit;
			}
			/* handle long args */
		} else {
			if (strcmp(&argv[i][2], "company") == 0) {
				if (argv[i+1] == NULL) {
					input_error(argv[0], "--company, but no company name given");
					ret = -1;
					goto exit;
				} else {
					printf("company: %s\n", argv[++i]);
					company = true;
				}
			} else if (strcmp(&argv[i][2], "file") == 0) {
				if (argv[i+1] == NULL) {
					input_error(argv[0], "--file, but no filename given");
					ret = -1;
					goto exit;
				} else {
					printf("file: %s\n", argv[++i]);
					from_file = true;
				}
			} else if (strcmp(&argv[i][2], "recursive") == 0) {
				printf("recursive\n");
				recursive = true;
			} else if (strcmp(&argv[i][2], "help") == 0) {
				usage();
				ret = -1;
			} else {
				input_error(argv[0], argv[i]);
				ret = -1;
				goto exit;
			}

		}
	}
	/* args were input correctly, now check if they meet program criteria */
	if (0 == ret)
		ret = check_args();

exit:
	return ret;
}

int input_error(char *prgm_name, char *err)
{
		if (err != NULL)
					printf("%s: invalid option -- '%s'\n", prgm_name, err);
			printf("Try '%s --help' for more information.\n", prgm_name);

				return 0;
}

int usage()
{
	printf("Usage: copyright_scrubber [OPTION]... [FILE]\n");
	printf("Removes the copyright from FILE.\n\n");
	printf("  -c, --company		company copyright to scrub\n");
	printf("  -f, --file		name of file to scrub\n");
	printf("  -r, --recursive	recursive scrub, starting with current directory\n");
	printf("      -h, --help	display this help and exit\n\n");
	printf("The company name and at least one of 'file' and 'recursive' are mandantory arguments\n");

	return 0;
}

