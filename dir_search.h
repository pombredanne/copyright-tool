#ifndef _DIR_SEARCH_H
#define _DIR_SEARCH_H

struct files_to_scrub locate_files(char *start_dir);
int check_file(struct dirent *dir, char *path);
char ** add_file(char **files, char *new_file, int file_count);

#endif /* _DIR_SEARCH_H */
