#!/usr/bin/python

###############################################################################
#
# This script recursively adds or removes copyrights from source files. It
# expects the phrasing to be such that the word "copyright" and the name of the
# company that holds the copyright are on the same line of the copyright
# clause.
# 
# Works for most *.c, *.cpp, *.h, *.bat, *.inc, *.reg, *.cmn, *.bib
# *.hpp, *.hxx, *.s, sources, makefile, *.def
#
# Written by Brent Generous, bgenerous@adeneo-embedded.us
#
# Updated: 05/09/2014
###############################################################################

import sys
import os
import getopt

# Globals
FORMAT_ERROR = 2
addingCopyright = False
removingCopyright = False

ADENEO_COPYRIGHT = 'Copyright (C) 2014, Adeneo Embedded All Rights Reserved.\n'

C_comments = ['.c', '.cpp', '.cxx', '.h', '.hpp', '.hxx'] # '/****/'
batch_comments = ['.bat'] # '@REM'
cmn_comments = ['.cmn'] # '!if 0'
reg_bib_inc_comments = ['.reg', '.bib', '.inc', '.def', '.s'] # ';'
make_comments = ['sour', 'make'] # '#'

def locateFiles(opts, files):
	fileTypes = ['.c', '.cpp', '.cxx', '.h', '.bat', '.inc', '.reg', '.cmn', '.bib', '.hpp', '.hxx', '.s', 'sour', 'make', '.def']
	for dirpath, dirnames, filenames in os.walk('.'):
		if '.git' in dirnames:
			dirnames.remove('.git')
		for filename in filenames:
			filename = os.path.join(dirpath, filename)
			if filename[-2:] in fileTypes or filename[-4:] in fileTypes:
				files.append(filename)

	return True
	
def addCopyright(sourceFile, fd, tmp, copyRight):
	if sourceFile[-2:] in C_comments:
		tmp.write('/*******************************************************************************\n')
		tmp.write('*\n')
		tmp.write('* ' + ADENEO_COPYRIGHT)
		tmp.write('*\n')
		tmp.write('*******************************************************************************/\n')
	elif sourceFile[-4:] in C_comments:
		tmp.write('/*******************************************************************************\n')
		tmp.write('*\n')
		tmp.write('* ' + ADENEO_COPYRIGHT)
		tmp.write('*\n')
		tmp.write('*******************************************************************************/\n')
	elif sourceFile[-4:] in batch_comments:
		tmp.write('@REM\n')
		tmp.write('@REM ' + ADENEO_COPYRIGHT)
		tmp.write('@REM\n')
	elif sourceFile[-4:] in cmn_comments:
		tmp.write('!if 0\n')
		tmp.write(';-------------------------------------------------------------------------------\n')
		tmp.write(';' + ADENEO_COPYRIGHT)
		tmp.write(';-------------------------------------------------------------------------------\n')
		tmp.write('!endif\n')
	elif sourceFile[-2:] in reg_bib_inc_comments:
		tmp.write(';-------------------------------------------------------------------------------\n')
		tmp.write(';\n')
		tmp.write(';' + ADENEO_COPYRIGHT)
		tmp.write(';\n')
		tmp.write(';-------------------------------------------------------------------------------\n')
	elif sourceFile[-4:] in reg_bib_inc_comments:
		if 'make' in sourceFile[:]:
			tmp.write('!if 0\n')
			tmp.write(';-------------------------------------------------------------------------------\n')
			tmp.write('; ' + ADENEO_COPYRIGHT)
			tmp.write(';-------------------------------------------------------------------------------\n')
			tmp.write('!endif\n')
		else:
			tmp.write(';-------------------------------------------------------------------------------\n')
			tmp.write(';\n')
			tmp.write(';' + ADENEO_COPYRIGHT)
			tmp.write(';\n')
			tmp.write(';-------------------------------------------------------------------------------\n')
	elif sourceFile[-4:] in asm_comments:
		tmp.write('#\n')
		tmp.write('#\n')
		tmp.write('# ' + ADENEO_COPYRIGHT)
		tmp.write('#\n')
		tmp.write('#\n')
	else:
		return False

	tmp.write('\n')

	# write rest of source file
	for line in fd:
		tmp.write(line)
	return True

def removeCopyright(fd, tmp, companyName):
	linesRemoved = []
	companyName_lwr = str.lower(companyName)
	while (True):
		line = fd.readline()
		# found first line of copyright
		if line.lower().find("copyright") != -1 and line.lower().find(companyName_lwr) != -1 or \
				line.lower().find("all rights reserved") != -1 and line.lower().find(companyName_lwr) != -1:
			linesRemoved.append(line)
			break

		# line is not part of copyright, hold on to it
		else:
			tmp.write(line)

		# check if hit EOF
		if not line:
			# no copyright found
			return False	
	
	# inside copyright
	while (True):
		line = fd.readline()
		# check if we reached end of copyright
		if str.lower("APPLICABLE LICENSE AGREEMENT") in str.lower(line) or \
				str.lower("POSSIBILITY OF SUCH DAMAGE") in str.lower(line)or \
				str.lower("SOFTWARE HAS BEEN SUPPLIED") in str.lower(line):
			linesRemoved.append(line)
			break

		# still in the comment, continue to skip lines
		else:
			linesRemoved.append(line)

		# check if hit EOF
		if not line:
			# never found matching copyright format
			return FORMAT_ERROR

	# write rest of file to tmp
	for line in fd:
		tmp.write(line)
	for line in linesRemoved:
		print("REMOVED: " + line),
	print("")

	return True


def saveChanges(fd, tmp):
	# write tmp back to original file
	fd.seek(0)
	for line in tmp:
		fd.write(line)

	return True


def checkArgs(args):
	if '-rf' in args or '-fr' in args:
		print("Must specify either recursive scrub or a file to scrub, but not both")
		return False
	elif ('-r' in args or '--recursive' in args) and ('-f' in args or '--file' in args):
		print("Must specify either recursive scrub or a file to scrub, but not both")
		return False
	elif '-c' not in args and '--company' not in args:
		print("Must specify company copyright to scrub")
		return False
	elif ('-r' not in args and '--recursive' not in args) and ('-f' not in args and '--file' not in args):
		print("Must specify either recursive scrub or a file to scrub")
		return False

	return True


def parseArgs(argv, opts, files, company):
	for opt, arg in opts:
		if opt in ('-r', '--recursive'):
			locateFiles(opts, files)
		elif opt in ('-c', '--company'):
			company = arg
		elif opt in ('-f', '--file'):
			files.append(arg)
		elif opt in ('-a', '--add'):
			global addingCopyright
			addingCopyright = True
		elif opt in ('-R', '--remove'):
			global removingCopyright
			removingCopyright = True
		elif opt in ('-h', '--help'):
			usage()
			sys.exit(1)

	if checkArgs(argv) == False:
		inputError()
		sys.exit(1)

	return company


def inputError(err=''):
	if err != '':
		print(err)
	print("Try 'copyright_scrubber --help' for more information.")


def usage():
	print("Usage: copyright_scrubber [OPTION]... [FILE]")
	print("Adds or removes the specified copyright from FILE.\n")
	print("  -a, --add		add copyright")
	print("  -R, --remove		remove copyright")
	print("  -c, --company		company copyright to scrub")
	print("  -f, --file		name of file to scrub")
	print("  -r, --recursive	recursive scrub, starting with current directory")
	print("      -h, --help		display this help and exit\n")
	print("The company name and at least one of 'file' and 'recursive' are mandantory arguments")


def main(argv=None):
	if argv is None:
		argv = sys.argv

	# will hold the name of the files we will scrub
	files = []
	# will hold the names of files that had errors
	formatError = []
	company = ''

	# get command line arguments
	try:
		opts, args = getopt.getopt(argv[1:], 'aRrc:f:h', ['add', 'remove', 'recursive', 'company=', 'file=', 'help'])
	except getopt.GetoptError as err:
		inputError(err)
		sys.exit(1)

	company =  parseArgs(argv, opts, files, company)
	if company == '':
		print("Company name: " + company + "is invalid")
		sys.exit(1)

	if removingCopyright == True:
		# check for and remove any matching copyrights
		scrubCount = 0
		for sourceFile in files:
			print("In file: " + sourceFile)
			tmpFile = "./tmp"

			try:
				fd = open(sourceFile, 'r')
			except IOError as err:
				print(err)
				break
			tmp = open(tmpFile, 'w')
			
			ret = removeCopyright(fd, tmp, company)
			if ret == False:
				print("No " + company + " copyright found\n")
			elif ret == FORMAT_ERROR:
				formatError.append(sourceFile)
				print("Copyright found, format unrecognized\n")
			else:
				fd.close()
				tmp.close()

				fd = open(sourceFile, 'w')
				tmp = open(tmpFile, 'r')

				saveChanges(fd, tmp)
				scrubCount += 1

			fd.close()
			tmp.close()
			os.remove(tmpFile)
			
		for entry in formatError:
			print("FORMAT ERROR in file: " + entry)
			print("*** Found unrecognized copyright format -- remove copyright manually ***\n")
	
		print("Out of " + str(len(files)) + " files searched, " + str(scrubCount) + " files were scrubbed.")
		print("Found " + str(len(formatError))  + " file(s) with unrecognized copyright format(s).")
	elif addingCopyright == True:
		addCount = 0
		for sourceFile in files:
			print("In file: " + sourceFile)
			tmpFile = "./tmp"

			try:
				fd = open(sourceFile, 'r')
			except IOError as err:
				print(err)
				break
			tmp = open(tmpFile, 'w')
			ret = addCopyright(sourceFile, fd, tmp, company)
			addCount += 1
			fd.close()
			tmp.close()

			fd = open(sourceFile, 'w')
			tmp  = open(tmpFile, 'r')

			saveChanges(fd, tmp)
			
			fd.close()
			tmp.close()
			os.remove(tmpFile)

		print("Added " + company + " copyright to " + str(addCount) + " files.")
	else:
		print("Must specify either to add copyright or remove copyright")
		inputError()

	return True

if __name__ == "__main__":
	sys.exit(main())
