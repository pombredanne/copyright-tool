/******************************************************************************
*
* Functions to recursively search through directories and return file names
* based on file extension.
*
* Written by Brent Generous, bgenerous@adeneo-embedded.us
******************************************************************************/

#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include "copyright_tool.h"
#include "dir_search.h"

const char *supp_types[NUM_SUPP_TYPES] = {".cpp", ".cxx", ".c", ".h", ".bat", \
					".inc",	".reg", ".cmn", ".bib"};

struct files_to_scrub locate_files(char *start_dir)
{
	int i = 0;
	int ret = 0;
	DIR *d;
	struct dirent *dir;
	char *rel_path = (char *) malloc(sizeof(*rel_path) * MAX_PATH_LEN);
	/* static so they remain accurate even with recursion */
	static char **files = NULL;
	static int file_count = 0;
	struct files_to_scrub filenames;

	d = opendir(start_dir);
	if (d) {
		while ((dir = readdir(d)) != NULL) {
			/* construct entry name as relative path, first reset relative path name */
			memset(rel_path, 0, MAX_PATH_LEN * sizeof(*rel_path));
			strcat(rel_path, start_dir);
			strcat(rel_path, "/");
			strcat(rel_path, dir->d_name);

			ret = check_file(dir, rel_path);
			if (ret == 0) {
				/* made it through the sieve */
				files = add_file(files, rel_path, file_count);
				file_count++;
			} else if (ret == 1) {
				locate_files(rel_path);
			}
		}
		closedir(d);
	}
	free(rel_path);

	filenames.files = files;
	filenames.file_count = file_count;

	return filenames;
}

int check_file(struct dirent *dir, char *path)
{
	DIR *r = NULL;
	int ret = -1;
	int filetype = 0;
	int i = 0;

	/* try to open entry, success => dir, failure => file */
	r = opendir(path);
	if (r) {
		if (strcmp(dir->d_name, ".") == 0) {
			/* skip this entry */
			ret = -1;
		} else if (strcmp(dir->d_name, "..") == 0) {
			/* skip this entry */
			ret = -1;
		} else if (strcmp(dir->d_name, ".git") == 0) {
			/* skip this entry */
			ret = -1;
		} else {
			/* this directory will be searched */
			closedir(r);
			ret = 1;
		}
	} else if (strlen(dir->d_name) < 3) {
		/* filename is invalid */ 
		ret = -1;
	} else {
		/* file extension is in the last 4 characters */
		filetype = strlen(path) - 4;
		for (i = 0; i < NUM_SUPP_TYPES; i++) {
			if (strstr(&path[filetype], supp_types[i])) {
				/* it's a supported filetype */
				ret = 0;
				break;
			}
		}
	}

	return ret;
}

char ** add_file(char **files, char *new_file, int file_count)
{
	char **tmp = NULL;

	/* realloc to current array size + one more element */
	tmp = (char **) realloc(files, (sizeof(*files) * file_count) + sizeof(*files));
	if (!tmp) {
		perror("Memory reallocation error: ");
		return NULL;
	}

	files = tmp;

	files[file_count] = (char *) malloc(sizeof(**files) * MAX_PATH_LEN);
	if (!files[file_count]) {
		perror("Memory allocation error: ");
		return NULL;
	}

	memset(files[file_count], 0 , sizeof(**files) * MAX_PATH_LEN);
	memcpy(files[file_count], new_file, MAX_PATH_LEN * sizeof(*new_file));

	return files;
}

