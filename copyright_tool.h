#ifndef _COPYRIGHT_SCRUBBER_H
#define _COPYRIGHT_SCRUBBER_H

#include <stdbool.h>

#define NUM_SUPP_TYPES	9	
#define MAX_PATH_LEN	256

struct files_to_scrub {
	char **files;
	int file_count;
};

extern bool recursive;
extern bool from_file;
extern bool company;

#endif/* _COPYRIGHT_SCUBBER_H */
